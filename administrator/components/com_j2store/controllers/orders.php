<?php
/**
 * @package J2Store
 * @copyright Copyright (c)2014-17 Ramesh Elamathi / J2Store.org
 * @license GNU GPL v3 or later
 */
// No direct access to this file
defined('_JEXEC') or die;

class J2StoreControllerOrders extends F0FController
{
	public function __construct($config) {
		parent::__construct($config);
		$this->registerTask('apply', 'save');
		$this->registerTask('saveNew', 'save');
	}


	/*
	 * Method to save Order status
	*/
	public function saveOrderstatus(){
		$data = $this->input->getArray($_POST);
		$id = $this->input->getInt('id');
		$status =false;
		$return = isset($data['return']) ? $data['return'] : '';
		$order_id = $this->input->getString('order_id');
		$order = F0FTable::getInstance('Order', 'J2StoreTable');
		$order->load(array('order_id'=>$order_id));

		if(!empty($order->order_id)) {

			//update status
			$order->update_status($data['order_state_id'], $data['notify_customer']);

			if(isset($data['reduce_stock']) && $data['reduce_stock'] == 1) {
				$order->reduce_order_stock();
			}

			if(isset($data['increase_stock']) && $data['increase_stock'] == 1) {
				$order->restore_order_stock();
			}

			if(isset($data['grant_download_access']) && $data['grant_download_access'] == 1) {
				$order->grant_download_permission();
			}

		}

		//is it an ajax call
		if($return){
			$json =array();
			$link = 'index.php?option=com_j2store&view=orders';
			$json['success']['link'] = $link;
			echo json_encode($json);
			JFactory::getApplication()->close();
		}else {
			$url ='index.php?option=com_j2store&view=order&task=edit&id='.$id;
			$this->setRedirect($url, $msg,$msgType);
		}

	}

	/**
	 * Method to save Order Customer Note
	 */
	public function saveOrderCnote(){
		$data = $this->input->getArray($_POST);
		$id = $this->input->getInt('id');
		$order = F0FTable::getAnInstance('Order' ,'J2StoreTable');
		$msg = JText::_('J2STORE_ORDER_SAVE_ERROR');
		$msgType='warning';
		//must check id exists
		if($id){
			//then load the id and confirm row exists
			if($order->load($id)){
				//now assign the customer note to order customer note object
				$order->customer_note = $data['customer_note'];
				$msg = JText::_('J2STORE_ORDER_SAVED_SUCCESSFULLY');
				$msgType ='message';
				if(!$order->save($order)){
					$msg = JText::_('J2STORE_ORDER_SAVE_ERROR');
					$msgType='warning';
				}
			}
		}
		$url ='index.php?option=com_j2store&view=order&task=edit&id='.$id;
		$this->setRedirect($url, $msg,$msgType);

	}

	/**
	 * Method to save shipping tracking id
	 */
	public function saveTrackingId(){
		$data = $this->input->getArray($_POST);
		$id = $this->input->getInt('id');
		$order = F0FTable::getAnInstance('Order' ,'J2StoreTable');
		$msg = '';
		$msgType='warning';

		//must check id exists
		if($order->load($id)){
			//load the shipping
			$ordershipping = F0FTable::getAnInstance('Ordershipping', 'J2StoreTable');

			if($ordershipping->load(array('order_id'=>$order->order_id))){
				$ordershipping->ordershipping_tracking_id = isset($data['ordershipping_tracking_id']) ? $data['ordershipping_tracking_id'] : '';
				if($ordershipping->store()) {
					$msg = JText::_('J2STORE_ORDER_SAVED_SUCCESSFULLY');
					$msgType ='message';
				}else {
					$msg = JText::_('J2STORE_ORDER_SAVE_ERROR');
					$msgType='warning';
				}
			}
		}
		$url ='index.php?option=com_j2store&view=order&task=edit&id='.$id;
		$this->setRedirect($url, $msg,$msgType);

	}

	/**
	 * Method to edit orderinfo based on the address type
	 *
	 */
	function setOrderinfo(){
		$order_id  = $this->input->getString('order_id');
		$address_type = $this->input->getString('address_type');
		$orderinfo = F0FTable::getAnInstance('Orderinfo','J2StoreTable');
		$orderinfo->load(array('order_id'=>$order_id));
		$type = "all_".$address_type;
		$custom_datas = json_decode($orderinfo->$type);
		
		$processed = $this->removePrefix((array)$orderinfo,$address_type);
		foreach($custom_datas as $key =>$custom_data){
			$processed->$key = $custom_data->value;
		}
		$model = F0FModel::getTmpInstance('Orders','J2StoreModel');
		$view = $this->getThisView();
		$view->setModel($model, true);
		$view->addTemplatePath(JPATH_ADMINISTRATOR.'/components/com_j2store/views/order/tmpl/');
		$view->set('address_type',$address_type);
		$fieldClass  = J2Store::getSelectableBase();
		$view->set('fieldClass' , $fieldClass);
		$view->set('orderinfo',$processed);
		$view->set('item',$orderinfo);
		$view->setLayout('address');
		$view = $this->display();
	}

	/**
	 * Method to save orderinfo
	 */
	function saveOrderinfo(){
		$data = $this->input->getArray($_POST);
		$order_id = $this->input->getString('order_id');
		$order = F0FTable::getAnInstance('Order','J2StoreTable');
		$order->load(array('order_id'=>$order_id));
		$address_type = $this->input->getString('address_type');
		$orderinfo = F0FTable::getAnInstance('Orderinfo','J2StoreTable');
		$orderinfo->load(array('order_id'=>$order_id));

		//$orderinfo->bind($data);
		$msg =JText::_('J2STORE_ORDERINFO_SAVED_SUCCESSFULLY');
		$msgType='message';
		$data['all_'.$address_type]= $order->processCustomFields($address_type, $data);
		if(!$orderinfo->save($data)){
			$msg =JText::_('J2STORE_ORDERINFO_SAVED_SUCCESSFULLY');
			$msgType='warning';
		}
		$url = "index.php?option=com_j2store&view=orders&task=setOrderinfo&order_id=".$order_id."&address_type=".$address_type."&layout=address&tmpl=component";
		$this->setRedirect($url, $msg,$msgType);

	}

	/**
	 * Method to remove the prefix and return result of address
	 * @param unknown_type $input
	 * @param unknown_type $prefix
	 */
	public function removePrefix($input ,$prefix) {
		$keys = array_keys($input);
		$values =array();
		$return = new JObject();
		foreach($input as $k =>$value){
			if (strpos($k,$prefix.'_') === 0){
				$key =  str_replace($prefix.'_','',$k);
				$return->$key = $value;
			}
		}

		return $return;
	}

	/**
	 * Method to get Countrylist
	 */
	public function getCountry(){
		$app = JFactory::getApplication();
		$country_id = $this->input->getInt('country_id');
		$zone_id = $this->input->getInt('zone_id');
		if($country_id) {
			$zones = F0FModel::getTmpInstance('Zones', 'J2storeModel')->country_id($country_id)->getList();
		}
		$json = array();
		$json['zone'] = $zones ;
		echo json_encode($json);
		$app->close();

	}


	function download() {
		$app = JFactory::getApplication();
		$ftoken = $app->input->getString('ftoken', '');

		if($ftoken) {
			$table = F0FTable::getInstance('Upload', 'J2StoreTable');
			if($table->load(array('mangled_name'=>$ftoken))) {
				$name = $table->original_name;
				$mask = basename($name);
				$file = $table->saved_name;
				jimport('joomla.filesystem.file');
				$path = JPATH_ROOT.'/media/j2store/uploads/'.$file;
				if(JFile::exists($path)) {
					F0FModel::getTmpInstance('Orderdownloads', 'J2StoreModel')->downloadFile($path, $mask);
					$app->close();
				}
			}
		}
	}



	public function printOrder(){
		$app = JFactory::getApplication();
		$order_id = $this->input->getString('order_id');
		$view = $this->getThisView();
		if ($model = $this->getThisModel())
		{
			// Push the model into the view (as default)
			$view->setModel($model, true);
		}
		$order = F0FTable::getInstance('Order' ,'J2StoreTable');
		$order->load(array('order_id' => $order_id));
		$error = false;
		$view->assign('order' ,$order );

		$view->assign('error', $error);
		$view->setLayout('print');
		$view->display();
	}


	public function printShipping(){
		$app = JFactory::getApplication();
		$order_id = $this->input->getString('order_id');
		$view = $this->getThisView();
		if ($model = $this->getThisModel())
		{
			// Push the model into the view (as default)
			$view->setModel($model, true);
		}

		$order = F0FTable::getInstance('Order' ,'J2StoreTable');
		$order->load(array('order_id' => $order_id));

		$orderinfo = F0FTable::getAnInstance('Orderinfo','J2StoreTable');
		$orderinfo->load(array('order_id'=>$order_id));

		$error = false;
		$view->assign('orderinfo' ,$orderinfo );
		$view->assign('item' ,$order );
		$view->assign('params' ,J2Store::config() );
		$view->assign('error', $error);
		$view->setLayout('print_shipping');
		$view->display();
	}
	
	/**
	 * Method to create or edit an existing order
	 *
	 */
	public function createOrder(){
		$option = $this->input->getCmd('option', 'com_j2store');
		$componentName = str_replace('com_', '', $option);
		$app = JFactory::getApplication();
		$session  = JFactory::getSession();
		$params = J2Store::config();
		
		$j2store_order_id = $this->input->getInt('oid',0);
		if($j2store_order_id == 0){
			$cid = $this->input->get('cid',array());
			if(isset($cid[0])){
				$j2store_order_id =$cid[0]; 
			}
		}
		$view = $this->getThisView('Orders');
		$sublayout = $app->input->getString('layout','basic');
		
		if ($model = $this->getThisModel())
		{
			// Push the model into the view (as default)
			$view->setModel($model, true);
		}
		$order = F0FTable::getInstance('Order' ,'J2StoreTable');
		$order->load($j2store_order_id);
		//get currency id, value and code and store it
		$currency = J2Store::currency();		
		$view->assign('order' ,$order );
		$view->assign('currency' ,$currency );
		$view->assign('params',$params);
		$fieldsets = array('basic' => JText::_('J2STORE_STORES_GROUP_BASIC'),
				'billing' => JText::_('J2STORE_BILLING_ADDRESS'),
				'shipping' => JText::_('J2STORE_SHIPPING_ADDRESS'),
				'items' => JText::_('J2STORE_ORDER_ITEMS'),
				'payment_shipping_methods'=> JText::_('J2STORE_PAYMENT_SHIPPING_METHODS'),
				'summary' => JText::_('J2STORE_ORDER_SUMMARY'),
		);
		$view->assign('fieldsets',$fieldsets);
		switch($sublayout){
			case 'basic':
				$update_history = 0;
				$native = JLanguageHelper::detectLanguage();
				if (empty($native))
				{
					$native = 'en-GB';
				}
				// Get the list of available languages.
				$languages_list = JLanguageHelper::createLanguageList($native);
				$languages = array();
				foreach ($languages_list as $language){
					$languages[$language['value']] = $language['text'];
				}
				if(empty($order->order_state_id)){
					$order->order_state_id = 5;
				}
				if(empty($order->order_id)){
					$update_history = 1;
				}
				$order_status = J2Html::getOrderStatusHtml($order->order_state_id);
				$view->assign('update_history',$update_history); 
				$view->assign('order_status',$order_status);
				$view->assign('languages',$languages);
				break;
			case 'billing':
				$orderinfo = $order->getOrderInformation();
				$address_model = F0FModel::getTmpInstance('Addresses', 'J2StoreModel');				
				$addresses = $address_model->user_id($order->user_id)->getList();
				$billing_processed = $this->removePrefix((array)$orderinfo,'billing');												
				$view->assign('orderinfo',$orderinfo);
				if($order->user_id) {
					$address = $address_model->user_id($order->user_id)->getFirstItem();
				} else {
					$address = F0FTable::getAnInstance('Address', 'J2StoreTable');
				}
				$view->assign('addresses',$addresses);					
				$view->assign('billing_address_id', $session->get('billing_address_id','','j2store'));
				$view->assign('fieldClass', J2Store::getSelectableBase());
				$view->assign('storeProfile', J2Store::storeProfile());
				$fields = J2Store::getSelectableBase()->getFields('billing',$address,'address');
				$view->assign('fields', $fields);
				$view->assign('address',F0FTable::getAnInstance('Address', 'J2StoreTable'));
				break;
			case 'shipping':
				$orderinfo = $order->getOrderInformation();
				$address_model = F0FModel::getTmpInstance('Addresses', 'J2StoreModel');
				$addresses = $address_model->user_id($order->user_id)->getList();
				$shipping_processed = $this->removePrefix((array)$orderinfo,'shipping');				
				$view->assign('orderinfo',$orderinfo);
				$view->assign('addresses',$addresses);
				$view->assign('shipping_address_id', $session->get('shipping_address_id','','j2store'));
				$view->assign('fieldClass', J2Store::getSelectableBase());
				$view->assign('storeProfile', J2Store::storeProfile());
				$fields = J2Store::getSelectableBase()->getFields('shipping',$address,'address');
				$view->assign('fields', $fields);
				$view->assign('address',F0FTable::getAnInstance('Address', 'J2StoreTable'));
				break;
				// let us load items
			case 'items':						
				//$taxes = $order->getOrderTaxRates();
				$orderitems = $order->getItems();
				//$view->assign('taxes',$taxes);				
				$view->assign('orderitems',$orderitems);
				break;
			case 'payment_shipping_methods':				
				$payment_plugins = J2Store::plugin()->getPluginsWithEvent( 'onJ2StoreGetPaymentPlugins' );						
				$default_method = !empty($order->orderpayment_type) ? $order->orderpayment_type : $params->get('default_payment_method', '');
				$plugins = array();
					
				if ($payment_plugins)
				{
					foreach ($payment_plugins as $plugin)
					{
						$results = $app->triggerEvent("onJ2StoreGetPaymentOptions", array( $plugin->element, $order ) );
						if (!in_array(false, $results, false))
						{
							if(!empty($default_method) && $default_method == $plugin->element) {
								$plugin->checked = true;
							}
							$plugins[] = $plugin;
						}
					}
				}
				$shipping = $order->getOrderShippingRate();
				$shipping_amount = $order->order_shipping + $order->order_shipping_tax;				
				$view->assign('shipping_name',$shipping->ordershipping_name);
				$view->assign('shipping_code',$shipping->ordershipping_code);
				$view->assign('shipping_amount',$shipping_amount);	
				$view->assign('shipping_plugin',$shipping->ordershipping_type);		
				$view->assign('paymentplugins',$plugins);
				break;
			
			case 'summary':
				
				$taxes = $order->getOrderTaxrates();
				
				$view->assign('taxes',$taxes);
				$view->assign('vouchers' , $order->getOrderVouchers());
				$view->assign('coupons' , $order->getOrderCoupons());
				$view->assign('shipping',$order->getOrderShippingRate());
				break;
		}
		$view->assign('form_prefix','jform');
		$view->assign('storeProfile',J2Store::storeProfile());
		$view->assign('layout',$sublayout);
		$view->setLayout('order');
		$view->display();
	}
		
	/**
	 * Method to save the Order step by step
	 * based on the layout
	 * switch to save function
	 * @return result array()
	 */
	
	public function saveAdminOrder(){
		$app = JFactory::getApplication();
		// get the session object
		$session = JFactory::getSession();
		$sublayout = $app->input->getString('layout','basic');
		$next_layout = $app->input->getString('next_layout','');
		$order_id = $this->input->getInt('oid',0);
		$order = F0FTable::getInstance('Order' ,'J2StoreTable');
		$order->load($order_id);		
		$result =array('msg' => JText::_('J2STORE_SAVE_SUCCESS') ,'msgType'=>'message');
		$data = $app->input->get('jform',array(),'ARRAY');
		
		switch($sublayout){
			// save basic function
			case 'basic':
				$result = $order->saveAdminOrderBasic($data);
				break;
			// save billing information
			case 'billing':
				//$address_type = $app->input->getString('address_type' ,'billing');
				$data = $app->input->getArray($_REQUEST);							
				$result = $order->saveAdminOrderInfo($data);
				break;
			//save shipping address
			case 'shipping':
				//$address_type = $app->input->getString('address_type' ,'shipping');
				$data = $app->input->getArray($_REQUEST);
				$result = $order->saveAdminOrderInfo($data);
				break;				
			case 'payment_shipping_methods':
				$shipping_name = $app->input->getString('shipping_name','');
					
				if(!empty($shipping_name)){	
					$post_data = $app->input->getArray($_REQUEST);					
					$values = array();
					$values['shipping_price'] = isset($post_data['shipping_price']) ? $post_data['shipping_price'] : 0;
					$values['shipping_extra'] = isset($post_data['shipping_extra']) ? $post_data['shipping_extra'] : 0;
					$values['shipping_tax'] = isset($post_data['shipping_tax']) ? $post_data['shipping_tax'] : 0;
					$values['shipping_code']= isset($post_data['shipping_code']) ? $post_data['shipping_code'] : 0;
					$values['shipping_name']= isset($post_data['shipping_name']) ? $post_data['shipping_name'] : '';
					$values['shipping_plugin']= isset($post_data['shipping_plugin']) ? $post_data['shipping_plugin'] : 'shipping_admin';
						
					$session->set('shipping_values',$values,'j2store');
				}					
				$order->orderpayment_type = $app->input->getString('payment_plugin','');
				$order->getAdminTotals();
				break;	
			case 'items':
				$order->getAdminTotals();
				$result['msg'] = JText::_('J2STORE_ORDER_ITEM_CHANGE_SUCCESS');
				break;
			case 'summary':
				$order->getAdminTotals();
				
				break;			
		}
		
		$url ='index.php?option=com_j2store&view=orders&task=createOrder&layout='.$sublayout.'&oid='.$order->j2store_order_id;		
		if($next_layout=="summary" && $sublayout=="summary"){
			$url ="index.php?option=com_j2store&view=order&id=".$order->j2store_order_id;
		}elseif($next_layout !=''){
			$url ='index.php?option=com_j2store&view=orders&task=createOrder&layout='.$next_layout.'&oid='.$order->j2store_order_id;
		}
		$this->setRedirect($url ,$result['msg'] , $result['msgType']);
	}
	function calculateTax(){
		$app = JFactory::getApplication();
		$order_id = $app->input->get('oid',0);
		$order = F0FTable::getInstance('Order' ,'J2StoreTable');
		$order->load($order_id);
		
		$order->getAdminTotals(true);
		$result['msg'] = JText::_('J2STORE_ORDER_ITEM_CHANGE_SUCCESS');
		$url ='index.php?option=com_j2store&view=orders&task=createOrder&layout=summary&oid='.$order->j2store_order_id;
		$json = array();
		$json['success']= 1;
		$json['redirect'] = $url;
		echo json_encode($json);
		$app->close();
	}
	/**
	 * validate order address
	 *   */
	public function validate_address(){
		$app = JFactory::getApplication();
		$data = $app->input->getArray($_POST);		
		$json = array();
		if(isset($data['order_id']) && isset($data['validate_type']) && $data['order_id'] && $data['validate_type']){
			$order = F0FTable::getInstance('Order' ,'J2StoreTable');
			$order->load(array(
					'order_id' => $data['order_id']
			));
			$data['email'] = $order->user_email;
			$data['admin_display_error'] = 1;
			$selectableBase = J2Store::getSelectableBase();			
			$json = $selectableBase->validate($data, $data['validate_type'], 'address');			
		}else{
			$json['error']['validate_type'] = JText::_('J2STORE_INVALID_ADDRESS_TYPE');
		}
		J2Store::plugin()->event('CheckoutValidateBilling',array(&$json));
		if(!$json){
			$json['success'] = 1;
		}
		echo json_encode($json);
		$app->close();
	}

	/**
	 * get product list in search
	 *    */
	public function getproducts(){
		$app = JFactory::getApplication();
		$q = $app->input->post->getString('q');
		$json = array();
		$model =F0FModel::getTmpInstance('Products','J2StoreModel');
		$model->setState('search',$q);
		$items= $model->getSFProducts();
		if(count($items)) {
			foreach($items as &$item) {
				F0FModel::getTmpInstance('Products', 'J2StoreModel')->runMyBehaviorFlag(true)->getProduct($item);
			}
		}
		echo json_encode($items);
		$app->close();
	}
		
	function removeOrderitem(){
		$app = JFactory::getApplication();
		$layout = $app->input->getString('layout','items');
		J2Store::utilities()->clear_cache();
		J2Store::utilities()->nocache();
		$order_id = $app->input->get('oid',0);
		$order = F0FTable::getInstance('Order' ,'J2StoreTable');
		$order->load($order_id);
	
		$orderitem_id = $app->input->get('orderitem_id',0);
		$url = 'index.php?option=com_j2store&view=orders&task=createOrder&layout='.$layout.'&oid='.$order_id;
		$orderItem = F0FTable::getAnInstance('OrderItem','J2StoreTable');
		$orderItem->load($orderitem_id);
		if(!empty($orderItem->cartitem_id)){
			$CartItem = F0FTable::getAnInstance('CartItem','J2StoreTable');
			$CartItem->delete($orderItem->cartitem_id);
			$orderitemattribute = F0FTable::getAnInstance('OrderItemAttribute', 'J2StoreTable')->getClone();
			$orderitemattribute->load(array(
					'orderitem_id' => $orderitem_id
			));
			if(isset($orderitemattribute->j2store_orderitemattribute_id) && $orderitemattribute->j2store_orderitemattribute_id >0){
				$orderitemattribute->delete();
			}
		}
		if($orderItem->delete()){
	
			$order->getAdminTotals();
	
			$msg = JText::_('J2STORE_ORDERITEM_DELETED_SUCCESSFULLY');
		}else {
			$msg = $orderItem->getError();
		}
		$this->setRedirect($url, $msg, 'message');
	}	
}