(function ($) {
    $(document).ready(function () {
        var Menu = '.JS-Mobile-Menu',
            List = '.JS-Mobile-Menu-List',
            $Toggle = $('.JS-Mobile-Menu-Toggle'),
            Duration = $(Menu).attr('data-duration') ? $(Menu).attr('data-duration') : 700,
            Media_Menu = $(Menu).attr('data-menu-media') ? $(Menu).attr('data-menu-media') : 768,
            Is_Animated = false;

        $Toggle.click(function () {
            if ($(window).width() < Media_Menu && !Is_Animated) {
                var This_Menu = $(this).parents(Menu);
                Is_Animated = true;

                This_Menu.toggleClass('Active');
                $(List, This_Menu).slideToggle(Duration).toggleClass('Active').promise().done(function () {
                    Is_Animated = false;
                });
                return false;
            }
        });

        $('body').click(function () {
            if ($(window).width() < Media_Menu && !Is_Animated) {
                Is_Animated = true;
                $(List).slideUp(Duration)
                    .removeClass('Active')
                    .promise()
                    .done(function () {
                        Is_Animated = false;
                    });
                $(Menu).removeClass('Active');
            }
        });

        $(window).resize(function () {
            Menu_Activation();
        });
        Menu_Activation();

        function Menu_Activation() {
            if ($(window).width() < Media_Menu) {
                $(List).hide().removeClass('Active');
                $(Menu).removeClass('Active');
            } else {
                $(List).show();
            }
        }
    });
})(jQuery);