$(document).on('mouseover', '.JS-Hover-Image', function () {
    $(this).parents('.Feedback,.Gallery,.Catalog-View').find('.JS-View-Image')
        .css("background-image", $(this).find('div').css('background-image')).attr('data-index', $(this).index());
});


$('.JS-Hover-Image').mouseover(function () {
    $(this).parents('.Feedback').find('.JS-View-Image')
        .css("background-image", $(this).css('background-image'));
});

$('.Preview').JS_Easy_Carousel({
    Autoscroll: 4000,
    Effect: 'fade',
    Previous: $('<svg class="Preview-Left JS-Carousel-Previous"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left"></use></svg>'),
    Next: $('<svg class="Preview-Right JS-Carousel-Next"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-right"></use></svg>'),
});

$('.Car-Tune').JS_Easy_Carousel({
    Autoscroll: 20000,
    Effect: 'fade',
    Previous: $('<svg class="Preview-Left JS-Carousel-Previous"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left"></use></svg>'),
    Next: $('<svg class="Preview-Right JS-Carousel-Next"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-right"></use></svg>'),
});

$('.Hits-Popular').JS_Easy_Carousel({
    Autoscroll: 5000,
    Effect: 'slide',
    Range: 4,
    Previous: $('<svg class="Hit-Left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left"></use></svg>'),
    Next: $('<svg class="Hit-Right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-right"></use></svg>'),
    Media: {
        Adaptive_0_400: {
            Range: 1
        },
        Adaptive_401_500: {
            Range: 2
        },
        Adaptive_501_700: {
            Range: 3
        }
    }
});

$('.Hits-Sales').JS_Easy_Carousel({
    Autoscroll: 7000,
    Effect: 'slide',
    Range: 4,
    Previous: $('<svg class="Hit-Left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left"></use></svg>'),
    Next: $('<svg class="Hit-Right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-right"></use></svg>'),
    Media: {
        Adaptive_0_400: {
            Range: 1
        },
        Adaptive_401_500: {
            Range: 2
        },
        Adaptive_501_700: {
            Range: 3
        }
    }
});

$('.Hits-News').JS_Easy_Carousel({
    Autoscroll: 9000,
    Effect: 'slide',
    Range: 4,
    Previous: $('<svg class="Hit-Left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left"></use></svg>'),
    Next: $('<svg class="Hit-Right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-right"></use></svg>'),
    Media: {
        Adaptive_0_400: {
            Range: 1
        },
        Adaptive_401_500: {
            Range: 2
        },
        Adaptive_501_700: {
            Range: 3
        }
    }
});

$('.JS-Portfolio-Carousel').JS_Easy_Carousel({
    Autoscroll: 9000,
    Effect: 'slide',
    Range: 4,
    Previous: $('<svg class="Hit-Left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left"></use></svg>'),
    Next: $('<svg class="Hit-Right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-right"></use></svg>'),
});


// Change Catalog Position
function Replace_Catalog() {
    if ($(window).width() < 800) {
        $('.Arena-Description').append($('.Sidebar-Cell'));
    } else {
        $('.Sidebar nav').prepend($('.Sidebar-Cell'));
    }
}

Replace_Catalog();
$(window).resize(function(){
    Replace_Catalog();
});