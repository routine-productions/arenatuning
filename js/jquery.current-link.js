/*
 * Copyright (c) 2015
 * Routine JS - Current Link
 * Version 0.1.0
 * Create 2015.12.04
 * Author Bunker Labs

 * Usage:
 *
 * Add class name 'JS-Current-Links' - to Box with navigation
 * Script will add class 'JS-Current-Link' to current link
 * and class 'JS-Current-Link-1' with same first chank

 * Code structure:
 * <div class="JS-Current-Links"><a href='/blog/post'></a></div>
 */
$(document).ready(function () {
    var $Current_Url = $('a'),
        Current_Link = 'JS-Current-Link',
        Current_Chunk_1 = 'JS-Current-Link-1',
        Current_Chunk_2 = 'JS-Current-Link-2';

    $Current_Url.each(function () {
        if($(this).attr('href')){
            var Link = $(this).attr('href').split('?')[0].replace(location.origin).replace(location.host);

            var Link_Chunks = Link.split('/');
            var Location_Chunks = location.pathname.split('/');




            if (Link_Chunks[1] == Location_Chunks[1]) {
                if (Link_Chunks[2] == Location_Chunks[2]) {
                    $(this).addClass(Current_Chunk_2);
                } else {
                    $(this).addClass(Current_Chunk_1);
                }
            }

            if (Link == location.pathname) {
                $(this).addClass(Current_Link);
                $(this).removeAttr('href');
            }
        }
    });


    $('.Sidebar-Catalog li>.JS-Current-Link').parent().addClass('Active');

    $('.Sidebar-Catalog ol>li>.JS-Current-Link').parents('ol').parents('li').addClass('Active');
});

