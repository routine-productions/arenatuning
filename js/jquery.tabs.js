/*
 * Copyright (c) 2015
 * Routine JS - Tabs
 * Version 0.1.3
 * Create 2015.12.17
 * Author Bunker Labs
 */

$(document).ready(function () {
    $('.JS-Tabs').each(function () {
        var Hash = $(this).find('.JS-Tabs-Navigation .Active').attr('href');
        $(this).find('.JS-Tabs-Content ' + Hash).show().siblings().hide();
    });


    $('.JS-Tabs .JS-Tabs-Navigation a').click(function () {
        var Hash = $(this).attr('href');
        $(this).parents('.JS-Tabs-Navigation').find('a').removeClass('Active');

        $(this).parents('.JS-Tabs').find('.JS-Tabs-Content ' + Hash).show().siblings().hide();
        $(this).addClass('Active');

        history.pushState(null, null, Hash);
        return false;
    });
});
