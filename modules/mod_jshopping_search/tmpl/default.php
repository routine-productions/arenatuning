<script type="text/javascript">
    function isEmptyValue(value) {
        var pattern = /\S/;
        return ret = (pattern.test(value)) ? (true) : (false);
    }
</script>

<form class="searchForm" name="searchForm" method="post"
      action="<?= SEFLink("index.php?option=com_jshopping&controller=search&task=result", 1); ?>"
      onsubmit="return isEmptyValue(jQuery('#jshop_search').val())">

    <input type="hidden" name="setsearchdata" value="1">
    <input type="hidden" name="category_id" value="<?= $category_id ?>"/>
    <input type="hidden" name="search_type" value="<?= $search_type; ?>"/>
    <input type="text" class="inputbox" name="search" id="jshop_search"
           value="<?= $search ?>" placeholder="Введите слово для поиска..."/>

    <input class="Hidden" type="submit" value="">
    <svg class="Search-Button">
        <use xlink:href="#search"></use>
    </svg>
</form>
<script>
    $('.Search-Button').click(function () {
        $('.searchForm [type=submit]').trigger('click');
    });
</script>
