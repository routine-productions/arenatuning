<h2 class="Hits-Header"><?= $module->title ?></h2>


<div class="Hits <?= json_decode($module->params)->moduleclass_sfx ?>">
    <ul class="Sale-Hits JS-Carousel-List">
        <?php foreach ($list as $item) { ?>
            <?php
            $Price = round(json_decode($item->source->attribs, true)['j2store']['price']);
            ?>

            <?php $image_intro = json_decode($item->source->images, true)['image_intro']; ?>
            <li class="Hit JS-Carousel-Item">
                <div class="Hit-View" style="background-image: url(<?= $image_intro ?>);"></div>
                <div class="Hit-Text">
                    <h3><?= $item->product_name ?></h3>
                    <p>Цена:<span class="Hit-Price"><?= $Price ?> p</span></p>
                </div>
                <a class="Hit-Button" href="<?php echo $item->link; ?>">Подробнее</a>
            </li>
        <?php } ?>
    </ul>
</div>