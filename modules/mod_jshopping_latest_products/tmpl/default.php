<h2 class="Hits-Header"><?= $module->title ?></h2>

<div class="Hits Hits-Slider <?= json_decode($module->params)->moduleclass_sfx ?>">
    <ul class="Sale-Hits JS-Carousel-List">
        <?php foreach ($rows as $item) { ?>

            <li class="Hit JS-Carousel-Item">
                <a href="<?= $item->product_link; ?>">
                    <div class="Hit-View" style="background-image: url(<?= $item->image ?>);"></div>
                    <div class="Hit-Text">
                        <h3><?= $item->name ?></h3>
                        <p>Цена:<span class="Hit-Price"><?= $item->product_price ?> p</span></p>
                    </div>
                </a>
                <a class="Hit-Button" href="<?php echo $item->product_link; ?>">Подробнее</a>
            </li>
        <?php } ?>
    </ul>
</div>