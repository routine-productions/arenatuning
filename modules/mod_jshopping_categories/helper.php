<?php

class jShopCategoriesHelper
{

    public static function getTreeCats($order, $ordering, $category_id, $categories_id, &$categories, $level = 0)
    {

        $cat = JTable::getInstance('category', 'jshop');
        $cat->category_parent_id = 0;
        $cats = $cat->getSisterCategories($order, $ordering);
        foreach ($cats as &$value) {
            $subCat = JTable::getInstance('category', 'jshop');
            $subCat->category_parent_id = $value->category_id;
            $subCat = $subCat->getSisterCategories($order, $ordering);
            $value->subcatgories = $subCat;

            foreach ($subCat as &$subValue) {
                $subCat = JTable::getInstance('category', 'jshop');
                $subCat->category_parent_id = $subValue->category_id;
                $subCat = $subCat->getSisterCategories($order, $ordering);
                $subValue->subcatgories = $subCat;
            }
        }

        $categories = $cats;


    }

    public static function getCatsArray($order, $ordering, $category_id, $categories_id = array())
    {
        $res_arr = array();
        jShopCategoriesHelper::getTreeCats($order, $ordering, $category_id, $categories_id, $res_arr, 0);
        return $res_arr;
    }

}
?>