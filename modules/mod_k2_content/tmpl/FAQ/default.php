<div class="Sidebar-Info FAQ">
    <h3>Вопросы и ответы</h3>

    <div class="Sidebar-Clients">
        <?php foreach ($items as $key => $item): ?>
        <a href="/support/">
            <h4><?= $item->title ?></h4>
            <p><?= $item->introtext ?></p>
        </a>
        <?php endforeach; ?>
    </div>
</div>