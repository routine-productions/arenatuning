<div class="Preview JS-Carousel">

    <div class="JS-Carousel-List">
        <?php foreach ($items as $key => $item): ?>
            <div class='JS-Carousel-Item' style="background-image: url(<?= "/media/k2/items/src/" . md5("Image" . $item->id) . ".jpg"; ?>)">
                <span><?= $item->title ?></span>
            </div>
        <?php endforeach; ?>
    </div>
</div>