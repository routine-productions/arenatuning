<div class="Sidebar-Info">
    <h3>Новые работы</h3>

    <div class="Sidebar-Clients">
        <?php foreach ($items as $key => $item): ?>
            <a href="/portfolio/">
                <h4><?= $item->title ?></h4>
                <p><?= $item->introtext ?></p>
                <div class="JS-Image-Align" data-image-ratio="16/10">
                    <img src="/media/k2/attachments/<?= $item->attachments[0]->filename ?>" alt="">
                </div>
            </a>
        <?php endforeach; ?>
    </div>
</div>
