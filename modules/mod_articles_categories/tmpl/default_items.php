<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_categories
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>


<?php foreach ($list as $item) : ?>
    <?php

    $image = json_decode($item->params)->image;
    ?>

    <li <?php if ($_SERVER['REQUEST_URI'] == JRoute::_(ContentHelperRoute::getCategoryRoute($item->id))) echo ' class="active"'; ?>> <?php $levelup = $item->level - $startLevel - 1; ?>

        <a href="<?php echo JRoute::_(ContentHelperRoute::getCategoryRoute($item->id)); ?>">
            <?php if (!empty($image)) { ?>
                <img src="<?= $image ?>">
            <?php } ?>

            <?php echo $item->title; ?>
        </a>

        <?php if (count($item->getChildren())): ?>
            <?php echo '<ol>'; ?>
            <?php $temp = $list; ?>
            <?php $list = $item->getChildren(); ?>
            <?php require JModuleHelper::getLayoutPath('mod_articles_categories', $params->get('layout', 'default') . '_items'); ?>
            <?php $list = $temp; ?>
            <?php echo '</ol>'; ?>
        <?php endif; ?>
    </li>
<?php endforeach; ?>

