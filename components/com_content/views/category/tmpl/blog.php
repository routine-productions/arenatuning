<div class="Catalog">
    <div class="Catalog-Items">
        <?php foreach ($this->lead_items as $item) { ?>
            <div class="Hit">
                <div class="Hit-View"
                     style="background-image: url(<?= json_decode($item->images)->image_intro ?>);"></div>
                <div class="Hit-Text">
                    <h3><?= $item->title ?></h3>
                    <p>Цена:<span class="Hit-Price"><?= round(json_decode($item->attribs)->j2store->price) ?> p</span></p>
                </div>
                <a class="Hit-Button" href="<?= JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catid)) ?>">Подробнее</a>
            </div>
        <?php } ?>
    </div>
</div>


