<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

//print_r($this->item);
//exit;
?>

<div class="Catalog">
    <section class="Catalog-Item">
        <div class="Catalog-View">
            <div class="View-Image"></div>
            <div class="View-Slider">
                <div class="View-Slides">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
        <section class="Catalog-Info">
            <h3><?= $this->item->title ?></h3>
            <ul>
                <li> юбка переднего бампера;</li>
                <li> юбка заднего бампера;</li>
                <li> реснички на передние фары;</li>
                <li> реснички на задние фонари</li>
                <li> Спойлер на 5 - ю дверь .</li>
            </ul>
            <p> Цена:<span class="Catalog-Price"> 155 000,00руб .</span></p>
                    <dl>
                        <dt>Реснички</dt>
                        <dd>
                            <select name="" id=""></select>
                            <svg>
                                <use xlink:href="#expand-more"></use>
                            </svg>
                            нет(-3 000,00 руб)
                        </dd>
                    </dl>
                    <p class="Catalog-Summery"><span> Цена с учетом выбранных опций:</span><span> 225 000,00руб .</span></p>
            <a class="Catalog-Button" href=""> Оформить заказ </a>
        </section>
    </section>

    <?php
    $document	= JFactory::getDocument();
    $renderer	= $document->loadRenderer('modules');
    $options	= array('style' => 'xhtml');
    $position	= 'about-item';
    echo $renderer->render($position, $options, null);

    ?>
</div>