<?php
/**
 * @version      4.9.1 13.08.2013
 * @author       MAXXmarketing GmbH
 * @package      Jshopping
 * @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
 * @license      GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
?>
<div class="Hits">
    <?php foreach ($this->rows as $k => $product) : ?>
        <?php include(dirname(__FILE__) . "/" . $product->template_block_product); ?>
    <?php endforeach; ?>
</div>