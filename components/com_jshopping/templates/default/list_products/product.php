<?php
/**
 * @version      4.8.0 05.11.2013
 * @author       MAXXmarketing GmbH
 * @package      Jshopping
 * @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
 * @license      GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
?>
<div class="Hit">
    <a href="<?= $product->product_link ?>">
        <div class="Hit-View" style="background-image: url(<?= $product->image ?>)"></div>
        <div class="Hit-Text">
            <h3><?= $product->name ?></h3>
            <p>Цена:<span class="Hit-Price"><?= $product->product_price ?> p</span></p>
        </div>
    </a>
    <a class="Hit-Button" href="<?= $product->product_link ?>">Подробнее</a>
</div>

