<?php
/**
 * @version      4.9.1 13.08.2013
 * @author       MAXXmarketing GmbH
 * @package      Jshopping
 * @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
 * @license      GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
?>
<div class="Form-Address">
    <?php
    $config_fields = $this->config_fields;
    include(dirname(__FILE__) . "/adress.js.php");
    ?>
    <form action="<?php print $this->action ?>" method="post" name="loginForm"
          onsubmit="return validateCheckoutAdressForm('<?= $this->live_path ?>', this.name)" autocomplete="off"
          enctype="multipart/form-data">
        <div class="Form-Control">
            <label for="l_name">ФИО</label>
            <input type="text" name="l_name" id="l_name" value="<?php print $this->user->l_name ?>"
                   class="input" placeholder="ФИО"/>
        </div>

        <div class="Form-Control">
            <label for="email">Email</label>
            <input type="text" name="email" id="email" value="<?php print $this->user->email ?>"
                   class="input" placeholder="Email"/>
        </div>

        <div class="Form-Control">
            <label for="phone">Телефон</label>
            <input type="text" name="phone" id="phone" value="<?php print $this->user->phone ?>"
                   class="input" placeholder="Телефон"/>
        </div>

        <div class="Form-Control">
            <label for="city">Адрес доставки</label>
            <input type="text" name="city" id="city" value="<?php print $this->user->city ?>"
                   class="input" placeholder="Адрес доставки"/>
        </div>

        <div class="Form-Control">
            <label for="ext_field_1">Сообщение</label>
                <textarea name="ext_field_1" id="ext_field_1"
                          class="input" placeholder="Сообщение"><?php print $this->user->ext_field_1 ?></textarea>
        </div>

        <input type="submit" name="next" value="<?php print _JSHOP_NEXT ?>" class="btn btn-primary button"/>

    </form>
</div>
