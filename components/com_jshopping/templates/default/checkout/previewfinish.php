<?php
defined('_JEXEC') or die('Restricted access');
?>

<?php print $this->small_cart ?>
<form class="Form-Address" name="form_finish" action="<?php print $this->action ?>" method="post"
      enctype="multipart/form-data">

    <input class="btn btn-primary button" type="submit" name="finish_registration"
           value="<?php print _JSHOP_ORDER_FINISH ?>"
           onclick="return checkAGBAndNoReturn('<?php echo $this->config->display_agb; ?>','<?php echo $this->no_return ?>');"/>
</form>

