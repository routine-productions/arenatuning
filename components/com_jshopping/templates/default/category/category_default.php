<?php
/**
 * @version      4.11.0 17.09.2015
 * @author       MAXXmarketing GmbH
 * @package      Jshopping
 * @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
 * @license      GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');

print $this->_tmp_category_html_start;
?>
<div class="Category">
    <h1 class="Category-Header"><?= $this->category->name ?></h1>




    <?php if (count($this->categories)) : ?>
        <div class="Category-List">
            <?php foreach ($this->categories as $k => $category) : ?>

                <div class="Category-Item">
                    <a href="<?php print $category->category_link; ?>">
                        <img class="Category-Item-Image" src="<?= $this->image_category_path; ?>/<?= $category->category_image ?>">
                        <span class="Category-Item-Title"><?= $category->name ?></span>

                    </a>

                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>


    <?php include(dirname(__FILE__) . "/products.php"); ?>


    <div class="Category-Description"><?= $this->category->description ?></div>

</div>