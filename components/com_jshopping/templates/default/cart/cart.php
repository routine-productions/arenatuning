<?php
//print_r($this->config->image_product_live_path);exit;
if (count($this->products) > 0) {
    ?>

    <div class="Basket">
        <div>
            <form action="<?= SEFLink('index.php?option=com_jshopping&controller=cart&task=refresh') ?>"
                  method="post" name="updateCart">
                <ul class="Basket-Head">
                    <li>Фото</li>
                    <li>Описание</li>
                    <li>Кол-во</li>
                    <li>Цена</li>
                    <li>Сумма</li>
                </ul>

                <?php foreach ($this->products as $key => $product) { ?>
                    <?php
//            print_r($product);exit;
                    ?>
                    <ul class="Basket-Row">
                        <li>
                            <div
                                style="background-image: url(<?= $this->config->image_product_live_path ?>/<?= $product['thumb_image'] ?>)"></div>
                        </li>
                        <li><?= $product['product_name'] ?></li>
                        <li>
                            <p>Кол-во:</p>
                <span>
                     <input type="number" max="100" name="quantity[<?= $key ?>]" value="<?= $product['quantity'] ?>"
                            class="inputbox"/>
                     <span onclick="document.updateCart.submit();">Обновить</span>
                </span>
                        </li>
                        <li class="Basket-Price"><p>Цена:</p><?= formatprice($product['price']) ?></li>
                        <li class="Basket-Summery"><p>
                                Сумма:</p><?= formatprice($product['price'] * $product['quantity']); ?></li>
                        <li>
                            <a href="<?= $product['href_delete'] ?>">
                                <svg>
                                    <use xlink:href="#close"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                <?php } ?>

                <div class="Basket-Bottom">
                    <p>Общая сумма заказа: <span class="Basket-Overall"><?= formatprice($this->summ); ?></span></p>
                    <a href="<?= $this->href_checkout ?>">Оформить заказ</a>
                </div>


            </form>
        </div>
    </div>
<?php } else { ?>
    <p class="Message">Корзина пуста</p>
<?php } ?>
