<?php
//print_r($this->config->image_product_live_path);exit;
?>

<div class="Basket">
    <div>
        <form action="<?= SEFLink('index.php?option=com_jshopping&controller=cart&task=refresh') ?>"
              method="post" name="updateCart">
            <ul class="Basket-Head">
                <li>Фото</li>
                <li>Описание</li>
                <li>Кол-во</li>
                <li>Цена</li>
                <li>Сумма</li>
            </ul>

            <?php foreach ($this->products as $key => $product) { ?>
                <?php
//            print_r($product);exit;
                ?>
                <ul class="Basket-Row">
                    <li>
                        <div
                            style="background-image: url(<?= $this->config->image_product_live_path ?>/<?= $product['thumb_image'] ?>)"></div>
                    </li>
                    <li><?= $product['product_name'] ?></li>
                    <li>
                        <p>Кол-во:</p>
                <span>
                      <?= $product['quantity'] ?>

                </span>
                    </li>
                    <li class="Basket-Price"><p>Цена:</p><?= formatprice($product['price']) ?></li>
                    <li class="Basket-Summery"><p>
                            Сумма:</p><?= formatprice($product['price'] * $product['quantity']); ?></li>
                    <li>
                    </li>
                </ul>
            <?php } ?>

            <div class="Basket-Bottom">
                <p>Общая сумма заказа: <span class="Basket-Overall"><?= formatprice($this->summ); ?></span></p>
            </div>


        </form>
    </div>

</div>
