<?php
defined('_JEXEC') or die('Restricted access');
$product = $this->product;
//print_r($product);exit;

include(dirname(__FILE__) . "/load.js.php");
?>
    <div class="Catalog">
        <form name="product" method="post" action="<?php print $this->action ?>" enctype="multipart/form-data"
              autocomplete="off">

            <section class="Catalog-Item">
                <h1><?= $product->name ?></h1>

                <div class="Catalog-View">
                    <div class="View-Image"
                         style="background-image: url(<?= $this->image_product_path . '/' . $this->images[0]->image_name; ?>)"></div>
                    <div class="View-Slider">
                        <div class="View-Slides">
                            <div>
                                <?php foreach ($this->images as $Image) { ?>
                                    <div style="background-image: url(<?= $this->image_product_path . '/' . $Image->image_name; ?>)"></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="Catalog-Info">
                    <?= $product->description ?>
                    <p>Цена:<span class="Catalog-Price"><?= $product->product_price ?> руб.</span></p>
                    <dl>

                    </dl>
                    <p class="Catalog-Summery prod_price"><span>Цена с учетом выбранных опций:</span><span id="block_price"><?= $product->product_price ?> руб.</span>
                    </p>
                    <a class="Catalog-Button" href="">Оформить заказ</a>
                </div>
            </section>


            <input type="hidden" name="to" id='to' value="cart"/>
            <input type="hidden" name="product_id" id="product_id" value="<?php print $this->product->product_id ?>"/>
            <input type="hidden" name="category_id" id="category_id" value="<?php print $this->category_id ?>"/>
        </form>

    </div>


    <div class="row-fluid jshop">
        <div class="span4 image_middle">


<?php if (count($this->attributes)) : ?>
    <div class="jshop_prod_attributes jshop">
        <?php foreach ($this->attributes as $attribut) : ?>
            <?php if ($attribut->grshow) { ?>
                <div>
                    <span class="attributgr_name"><?php print $attribut->groupname ?></span>
                </div>
            <?php } ?>
            <div class="row-fluid">
                <div class="span2 attributes_title">
                    <span class="attributes_name"><?php print $attribut->attr_name ?>:</span>
                    <span class="attributes_description"><?php print $attribut->attr_description; ?></span>
                </div>
                <div class="span10">
                            <span id='block_attr_sel_<?php print $attribut->attr_id ?>'>
                                <?php print $attribut->selects ?>
                            </span>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>



<?php print $this->_tmp_product_html_after_atributes; ?>

<?php if (count($this->product->freeattributes)) { ?>
    <div class="prod_free_attribs jshop">
        <?php foreach ($this->product->freeattributes as $freeattribut) { ?>
            <div class="row-fluid">
                <div class="span2 name">
                    <span class="freeattribut_name"><?php print $freeattribut->name; ?></span>
                    <?php if ($freeattribut->required) { ?><span>*</span><?php } ?>
                    <span class="freeattribut_description"><?php print $freeattribut->description; ?></span>
                </div>
                <div class="span10 field">
                    <?php print $freeattribut->input_field; ?>
                </div>
            </div>
        <?php } ?>
        <?php if ($this->product->freeattribrequire) { ?>
            <div class="requiredtext">* <?php print _JSHOP_REQUIRED ?></div>
        <?php } ?>
    </div>
<?php } ?>

<?php print $this->_tmp_product_html_after_freeatributes; ?>

<?php if ($this->product->product_is_add_price) { ?>
    <div class="price_prod_qty_list_head"><?php print _JSHOP_PRICE_FOR_QTY ?></div>
    <table class="price_prod_qty_list">
        <?php foreach ($this->product->product_add_prices as $k => $add_price) { ?>
            <tr>
                <td class="qty_from"
                    <?php if ($add_price->product_quantity_finish == 0){ ?>colspan="3"<?php } ?>>
                    <?php if ($add_price->product_quantity_finish == 0) print _JSHOP_FROM ?>
                    <?php print $add_price->product_quantity_start ?>
                    <?php print $this->product->product_add_price_unit ?>
                </td>

                <?php if ($add_price->product_quantity_finish > 0) { ?>
                    <td class="qty_line"> -</td>
                <?php } ?>

                <?php if ($add_price->product_quantity_finish > 0) { ?>
                    <td class="qty_to">
                        <?php print $add_price->product_quantity_finish ?><?php print $this->product->product_add_price_unit ?>
                    </td>
                <?php } ?>

                <td class="qty_price">
                            <span id="pricelist_from_<?php print $add_price->product_quantity_start ?>">
                                <?php print formatprice($add_price->price) ?><?php print $add_price->ext_price ?>
                            </span>
                    <span class="per_piece">/ <?php print $this->product->product_add_price_unit ?></span>
                </td>
                <?php print $add_price->_tmp_var ?>
            </tr>
        <?php } ?>
    </table>
<?php } ?>

<?php if ($this->product->product_old_price > 0) { ?>
    <div class="old_price">
        <?php print _JSHOP_OLD_PRICE ?>:
                <span class="old_price" id="old_price">
                    <?php print formatprice($this->product->product_old_price) ?>
                    <?php print $this->product->_tmp_var_old_price_ext; ?>
                </span>
    </div>
<?php } ?>

<?php if ($this->product->product_price_default > 0 && $this->config->product_list_show_price_default) { ?>
    <div class="default_price"><?php print _JSHOP_DEFAULT_PRICE ?>: <span
            id="pricedefault"><?php print formatprice($this->product->product_price_default) ?></span></div>
<?php } ?>

<?php print $this->_tmp_product_html_before_price; ?>

<?php if ($this->product->_display_price) { ?>
    <div class="prod_price">
        <?php print _JSHOP_PRICE ?>:
                <span id="block_price">
                    <?php print formatprice($this->product->getPriceCalculate()) ?>
                    <?php print $this->product->_tmp_var_price_ext; ?>
                </span>
    </div>
<?php } ?>

<?php print $this->product->_tmp_var_bottom_price; ?>

<?php if ($this->config->show_tax_in_product && $this->product->product_tax > 0) { ?>
    <span class="taxinfo"><?php print productTaxInfo($this->product->product_tax); ?></span>
<?php } ?>

<?php if ($this->config->show_plus_shipping_in_product) { ?>
    <span class="plusshippinginfo"><?php print sprintf(_JSHOP_PLUS_SHIPPING, $this->shippinginfo); ?></span>
<?php } ?>

<?php if ($this->product->delivery_time != '') { ?>
    <div class="deliverytime"
         <?php if ($product->hide_delivery_time){ ?>style="display:none"<?php } ?>><?php print _JSHOP_DELIVERY_TIME ?>
        : <?php print $this->product->delivery_time ?></div>
<?php } ?>

<?php if ($this->config->product_show_weight && $this->product->product_weight > 0) { ?>
    <div class="productweight"><?php print _JSHOP_WEIGHT ?>: <span
            id="block_weight"><?php print formatweight($this->product->getWeight()) ?></span></div>
<?php } ?>

<?php if ($this->product->product_basic_price_show) { ?>
    <div class="prod_base_price"><?php print _JSHOP_BASIC_PRICE ?>: <span
            id="block_basic_price"><?php print formatprice($this->product->product_basic_price_calculate) ?></span>
        / <?php print $this->product->product_basic_price_unit_name; ?></div>
<?php } ?>

<?php print $this->product->_tmp_var_bottom_allprices; ?>

<?php if (is_array($this->product->extra_field)) { ?>
    <div class="extra_fields">
        <?php foreach ($this->product->extra_field as $extra_field) { ?>
            <?php if ($extra_field['grshow']) { ?>
                <div class='block_efg'>
                <div class='extra_fields_group'><?php print $extra_field['groupname'] ?></div>
            <?php } ?>

            <div class="extra_fields_el">
                            <span
                                class="extra_fields_name"><?php print $extra_field['name']; ?></span><?php if ($extra_field['description']) { ?>
                    <span class="extra_fields_description">
                    <?php print $extra_field['description']; ?>
                    </span><?php } ?>:
                    <span class="extra_fields_value">
                        <?php print $extra_field['value']; ?>
                    </span>
            </div>

            <?php if ($extra_field['grshowclose']) { ?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
<?php } ?>

<?php print $this->_tmp_product_html_after_ef; ?>

<?php if ($this->product->vendor_info) { ?>
    <div class="vendorinfo">
        <?php print _JSHOP_VENDOR ?>: <?php print $this->product->vendor_info->shop_name ?>
        (<?php print $this->product->vendor_info->l_name . " " . $this->product->vendor_info->f_name; ?>),
        (
        <?php if ($this->config->product_show_vendor_detail) { ?><a
            href="<?php print $this->product->vendor_info->urlinfo ?>"><?php print _JSHOP_ABOUT_VENDOR ?></a>,<?php } ?>
        <a href="<?php print $this->product->vendor_info->urllistproducts ?>"><?php print _JSHOP_VIEW_OTHER_VENDOR_PRODUCTS ?></a>
        )
    </div>
<?php } ?>

<?php if (!$this->config->hide_text_product_not_available) { ?>
    <div class="not_available" id="not_available"><?php print $this->available ?></div>
<?php } ?>

<?php if ($this->config->product_show_qty_stock) { ?>
    <div class="qty_in_stock">
        <?php print _JSHOP_QTY_IN_STOCK ?>:
        <span id="product_qty"><?php print sprintQtyInStock($this->product->qty_in_stock); ?></span>
    </div>
<?php } ?>

<?php print $this->_tmp_product_html_before_buttons; ?>

<?php if (!$this->hide_buy) { ?>
    <div class="prod_buttons" style="<?php print $this->displaybuttons ?>">

        <div class="prod_qty">
            <?php print _JSHOP_QUANTITY ?>:
        </div>

        <div class="prod_qty_input">
            <input type="text" name="quantity" id="quantity" onkeyup="reloadPrices();" class="inputbox"
                   value="<?php print $this->default_count_product ?>"/><?php print $this->_tmp_qty_unit; ?>
        </div>

        <div class="buttons">
            <input type="submit" class="btn btn-primary button" value="<?php print _JSHOP_ADD_TO_CART ?>"
                   onclick="jQuery('#to').val('cart');"/>
        </div>

        <div id="jshop_image_loading" style="display:none"></div>
    </div>
<?php } ?>