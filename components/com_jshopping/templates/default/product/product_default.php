<?php
defined('_JEXEC') or die('Restricted access');
$product = $this->product;
include(dirname(__FILE__) . "/load.js.php");
?>
<div class="Catalog">
    <form name="product" method="post" action="<?php print $this->action ?>" enctype="multipart/form-data"
          autocomplete="off">

        <section class="Catalog-Item">
            <h1><?= $product->name ?></h1>

            <div class="Catalog-View">
                <div class="View-Image JS-View-Image JS-Window-Button" data-window-carousel='.JS-Hover-Image div'
                     style="background-image: url(<?= $this->image_product_path . '/' . $this->images[0]->image_name; ?>)"></div>

                <?php if (count($this->images) > 1) { ?>
                    <div class="View-Slider JS-Portfolio-Carousel">
                        <div class="View-Slides JS-Carousel-List">
                            <?php foreach ($this->images as $Image) { ?>
                                <div class="JS-Hover-Image JS-Carousel-Item JS-Window-Button"
                                     data-window-carousel='.JS-Hover-Image div'>
                                    <div
                                        style="background-image: url(<?= $this->image_product_path . '/' . $Image->image_name; ?>)"></div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="Catalog-Info">
                <?= $product->description ?>

                <?php if (count($this->attributes)) { ?>
                    <dl>
                        <?php foreach ($this->attributes as $attribut) : ?>
                            <dt><?= $attribut->attr_name ?></dt>
                            <dd><?= $attribut->selects ?></dd>
                        <?php endforeach; ?>
                    </dl>
                <?php } ?>

                <div class="prod_price">Цена: <span class="Catalog-Price"
                                                    id="block_price"><?= $this->product->getPriceCalculate() ?>
                        руб.</span></div>

                <input class="Catalog-Button" type="submit" class="btn btn-primary button" value="Оформить заказ"
                       onclick="jQuery('#to').val('cart');"/>
            </div>
        </section>

        <?php
        //        $document = JFactory::getDocument();
        //        $renderer = $document->loadRenderer('modules');
        //        $options = array('style' => 'xhtml');
        //        $position = 'about-item';
        //        echo $renderer->render($position, $options, null);
        require_once(__DIR__ . "/../../../../../includes/sxgeo.php");
        $SxGeo = new SxGeo('SxGeoCity.dat');
        $City = $SxGeo->getCity($_SERVER['REMOTE_ADDR']);
        ?>


        <section class="Catalog-Delivery JS-Tabs">
            <h2 class="JS-Tabs-Navigation">
                <a href="#Payment">Оплата</a>
                <a href="#Delivery" class="Active">Доставка</a>
                <?php if ($product->description) { ?>
                    <a href="#Description">Описание</a>
                <?php } ?>
            </h2>

            <div class="JS-Tabs-Content">
                <div id="Payment">
                    <p>При установке дополнительного оборудования, сигнализаций, элементов тюнинга, покраске, ТО и
                        кузовном ремонте в нашем сервисе, у Вас сохраняется гарантия на автомобиль.</p>
                    <p>Установка дополнительного оборудования и ремонт, который производится у нас, не является причиной
                        снятия с гарантийного обслуживания Вашего нового автомобиля (гарантии производителя), так как мы
                        являемся сертифицированным центром установки дополнительного оборудования. По запросу клиентов
                        выдаются копии сертификатов.</p>
                </div>

                <div class="Active" id="Delivery">
                    <p>В город <?= $City['city']['name_ru'] ?> и по области, а также другие регионы РФ,
                        товар доставляется курьером или
                        самовывозом. В случае Вашего нахождения в другом регионе России мы отправляем груз транспортными
                        компаниями или по почте. Расширяя дилерскую сеть мы стремимся к повышению эффективности работы с
                        магазинами тюнинга и оптовыми продавцами автомобильных аксессуаров. Нашим представителям и
                        региональным дилерам для удобства пользования сайтом рекомендуем зарегистрироваться. Процедура
                        регистрации занимает не более 2 - 3 минут, но это дает возможность на стадии формирования
                        заказа, не открывая прайс, видеть дилерские цены, а также получать сообщения о новинках и
                        спецпредложениях . Условия нашего сотрудничества не обязывают приобретать обвесы оптом. Для
                        предоставления льготных условий закупок достаточно быть нашим постоянным и надежным партнером.
                        Мы заинтересованы в обоюдно выгодных условиях сотрудничества!</p>
                    <p>Определяйтесь с составом заказа. Оформляйте заказ на сайте. При заказе обязательно укажите точный
                        адрес доставки, свою фамилию имя отчество, номер телефона, модель вашего автомобиля . Получаете
                        счет на ваш почтовый ящик. После подтверждения заказа менеджером,распечатывайте его(или
                        перепишите реквизиты) и оплачивайте в любом банке. После чего ожидайте получения долгожданного
                        комплекта от 5 до 15 дней(в среднем).</p>
                    <p>Юридические и физические лица получают все необходимые финансовые документы.</p>
                    <p>Цены на товары уже включают все налоги, но не включают стоимость доставки. При отправке по жд
                        груз страхуется. При отправке компаниями грузоперевозок - груз страхуется по желанию клиента.
                        Стоимость доставки вы можете рассчитать самостоятельно на сайтах компаний грузоперевозок,
                        указанных ниже.</p>
                </div>

                <?php if ($product->description) { ?>
                    <div id="Description">
                        <?= $product->description ?>
                    </div>
                <?php } ?>
            </div>

        </section>


        <input type="hidden" name="to" id='to' value="cart"/>
        <input type="hidden" name="product_id" id="product_id" value="<?php print $this->product->product_id ?>"/>
        <input type="hidden" name="category_id" id="category_id" value="<?php print $this->category_id ?>"/>
    </form>

</div>