<div class="Gallery-Wrap">

    <?php foreach ($this->leading as $key => $item) { ?>
        <article class="Gallery">
            <div>
                <h4><em><?= $item->title ?></em></h4>

                <p><?= $item->introtext ?></p>
            </div>

            <div>
                <div class="View-Image JS-View-Image JS-Window-Button" data-window-carousel='.JS-Hover-Image.Key-<?= $key ?> div'
                     style="background-image:url('/media/k2/attachments/<?= $item->attachments[0]->filename ?>');)">
                </div>

                <div class="View-Slider JS-Portfolio-Carousel">
                    <div class="View-Slides JS-Carousel-List">
                        <?php foreach ($item->attachments as $image) { ?>
                            <div class="Key-<?=$key ?> JS-Hover-Image JS-Carousel-Item JS-Window-Button"
                                 data-window-carousel='.JS-Hover-Image.Key-<?= $key ?> div'>
                                <div
                                    style="background-image:url('/media/k2/attachments/<?= $image->filename ?>');)"></div>
                            </div>
                        <?php } ?>
                    </div>
                </div>

            </div>
        </article>

    <?php } ?>

</div>