<div class="New">
    <article>
        <div>
            <img src="<?= "/media/k2/items/src/" . md5("Image" . $this->item->id) . ".jpg"; ?>" alt="">
        </div>
        <div>
            <h4><?= $this->item->title ?></h4>

            <?= $this->item->introtext ?>
        </div>
        <span class="New-Date"><?= substr($this->item->publish_up,0,10)?></span>
    </article>
</div>