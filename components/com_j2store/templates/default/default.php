<div class="Catalog">
    <nav>
        <div class="Catalog-Logo">
            <div></div>
        </div>
        <ul>
            <li><a href="/">Главная</a></li>
            <li><a href="catalog-marks">Range Rover</a></li>
            <li><a href="catalog-mark">Range Rover Sport</a></li>
            <li><a class="Active" href="catalog-items">Аэродинамические обвесы</a></li>
        </ul>
    </nav>
    <div class="Catalog-Items">
        <?php foreach ($this->products as $product) { ?>
            <?php print_r($product);?>
            <div class="Hit">
                <div class="Hit-View"
                     style="background-image: url(<?= json_decode($product->images)->image_intro ?>);"></div>

                <div class="Hit-Text">
                    <h3><?= $product->title ?></h3>
                    <p>Цена:<span class="Hit-Price"><?= round($product->price) ?> p</span></p>
                </div>

                <a class="Hit-Button" href="<?= $product->product_link ?>">Подробнее</a>
            </div>
        <?php } ?>
    </div>
</div>

