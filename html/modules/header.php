<header>
    <div class="Header-Top">
        <div class="Header-Logo">
            <a href="/">
                <img class="Header-Image" src="/img/pic/logo-arena.png" alt=""/>

                <h1><img src="/img/pic/arena.png" alt="Арена"/>
                    <small>Тюнинг</small>
                </h1>
            </a>
        </div>
        <div class="Header-Info">
            <div class="Header-Call">
                <svg>
                    <use xlink:href="#phone"></use>
                    <dl>
                        <dt>Позвоните нам</dt>
                        <dd>8 (495) 902-53-83</dd>
                    </dl>
                </svg>
            </div>
            <div class="Header-Work">
                <svg>
                    <use xlink:href="#clock"></use>
                </svg>
                <dl>
                    <dt>Время работы</dt>
                    <dd>Пн-Вс 9:00 - 19:00</dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="Header-Bottom">
        <nav>
            <ul>
                <li><a href="/">Главная</a></li>
                <li><a href="/company/">О компании</a></li>
                <li><a href="/payment/">Оплата и доставка</a></li>
                <li><a href="/montage/">Установка и покраска</a></li>
                <li><a href="/gallery/">Портфолио</a></li>
                <li><a href="/contacts/">Контакты</a></li>
            </ul>
            <div class="Header-Right">
                <div class="Header-Search">
                    <input type="text" placeholder="Введите слово для поиска..."/>
                    <svg>
                        <use xlink:href="#search"></use>
                    </svg>

                </div>
                <a href="/basket/">
                    <svg>
                        <use xlink:href="#basket"></use>
                    </svg>
                </a>
            </div>
        </nav>
    </div>

</header>