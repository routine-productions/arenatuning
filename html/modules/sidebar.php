<aside class="Sidebar">
    <nav>
        <div class="Sidebar-Cell">
            <h3>Каталог товаров</h3>
            <ul class="Sidebar-Catalog">
                <li>
                    <img src="/img/pic/audi.png" alt=""/>Audi
                </li>
                <li>
                   <img src="/img/pic/bmw.png"/>BMW

                </li>
                <li>
                   <img src="/img/pic/hummer.png"/>HUMMER

                </li>
                <li>
                   <img src="/img/pic/infinity.png"/>Infinity

                </li>
                <li>
                    <img src="/img/pic/land-rover.png"/>Land Rover

                    <ol>

                        <li>
                            <a href="/catalog-marks/">Discovery 3</a>
                        </li>
                        <li>
                            <a href="/catalog-marks/">Discovery 4</a>
                        </li>
                        <li>
                            <a href="/catalog-marks/">Range Rover Sport</a>
                        </li>
                    </ol>
                </li>
                <li>
                   <img src="/img/pic/lexus.png"/>Lexus
                </li>
            </ul>
            <ul class="Sidebar-Catalog">
                <li>
                   <img src="/img/pic/mazda.png"/>Mazda
                </li>
                <li>
                    <img src="/img/pic/mercedes.png"/>Mercedes-Benz
                </li>
                <li>
                    <img src="/img/pic/nissan.png"/>Nissan
                </li>
                <li>
                   <img src="/img/pic/porsсhe.png"/>Porsсhe
                </li>
                <li>
                   <img src="/img/pic/toyota.png"/>Toyota
                </li>
                <li>
                   <img src="/img/pic/volkswagen.png"/>Volkswagen
                </li>
            </ul>
        </div>
        <div class="Sidebar-Info">
            <h3>Информация</h3>
            <ul>
                <li>
                    <a href="/stock/">
                        <svg>
                            <use xlink:href="#stock"></use>
                        </svg>
                        Акции</a>

                </li>
                <li>

                    <a href="/feedback/">
                        <svg>
                            <use xlink:href="#comment"></use>
                        </svg>
                        Отзывы клиентов</a>

                </li>
                <li>

                    <a href="/news/">
                        <svg>
                            <use xlink:href="#chat-bubble"></use>
                        </svg>
                        Новости</a>

                </li>
                <li>

                    <a href="support">
                        <svg>
                            <use xlink:href="#forum"></use>
                        </svg>
                        F.A.Q.</a>
                </li>
            </ul>

        </div>

    </nav>
</aside>