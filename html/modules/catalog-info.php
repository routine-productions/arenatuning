<section class="Catalog-Info">
    <h3>Комплект стайлинга TechArt Porshe Cayenne 957. В состав комплекта входит:</h3>
    <ul>
        <li>юбка переднего бампера;</li>
        <li>юбка заднего бампера;</li>
        <li>реснички на передние фары;</li>
        <li>реснички на задние фонари</li>
        <li>Спойлер на 5-ю дверь.</li>
    </ul>
    <p>Цена:<span class="Catalog-Price">155 000,00руб.</span></p>
    <dl>
        <?php
        for ($Index = 0; $Index < 3; $Index++) {
            require './modules/catalog-info-item.php';
        }
        ?>
    </dl>
    <p class="Catalog-Summery"><span>Цена с учетом выбранных опций:</span><span >225 000,00руб.</span></p>
    <a class="Catalog-Button" href="">Оформить заказ</a>
</section>

