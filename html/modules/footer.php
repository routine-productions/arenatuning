<footer>
    <div class="Footer-Top">
        <div class="Footer-Left">
            <ul>
                <li><a href="/">Главная</a></li>
                <li><a href="/company/">О компании</a></li>
                <li><a href="/contacts/">Контакты</a></li>
            </ul>
            <ul>
                <li><a href="/payment/">Оплата и доставка</a></li>
                <li><a href="/montage/">Установка и покраска</a></li>
                <li><a href="/gallery/">Портфолио</a></li>
            </ul>
            <a href="/" class="Footer-Logo">
                <img class="Footer-Image" src="/img/pic/logo-arena.png" alt=""/>

                <h1>
                    <img src="/img/pic/arena.png" alt="Арена"/>
                    <small>Тюнинг</small>
                </h1>
            </a>
        </div>
        <div class="Footer-Right">
            <div>
                <a href="">
                    <svg>
                        <use xlink:href="#room"></use>
                    </svg>
                    <span>Московская обл., Мытищи, ул. Колпакова д. 2.</span>
                </a>
            </div>
            <div>
                <a href="">
                    <svg>
                        <use xlink:href="#phone"></use>
                    </svg>
                    <span>+7 (495) 902-53-83 (9:00 - 19:00)</span>
                </a>
            </div>
            <div>
                <a href="">
                    <svg>
                        <use xlink:href="#near"></use>
                    </svg>
                    <span>Схема проезда</span>
                </a>
            </div>
            <div>
                <a href="">
                    <svg>
                        <use xlink:href="#email"></use>
                    </svg>
                    <span>zakaz@arenatuning.ru</span>
                </a>
            </div>
        </div>
    </div>
    <div class="Copyright-Wrap">
        <div class="Copyright">
            <span>© 2016. Арена - Тюнинг. Все права защищены.</span>
            <a href="http://progress-time.ru/">Сделано в Progress Time</a>
        </div>
    </div>
</footer>