<div class="Basket">
    <nav>
        <ul>
            <li><a href="/">Главная</a></li>
            <li><a class="Active" href="/basket/">Корзина</a></li>
        </ul>
    </nav>
    <div>
        <ul class="Basket-Head">
            <li>Фото</li>
            <li>Описание</li>
            <li>Кол-во</li>
            <li>Цена</li>
            <li>Сумма</li>
        </ul>
<!--        BASKET-ROW-->
        <?php
        for ($Index = 0; $Index < 3; $Index++) {
            require './modules/basket-row.php';
        }
        ?>
        <div class="Basket-Bottom">
            <p>Общая сумма заказа: <span class="Basket-Overall">998 125,00 руб.</span></p>
            <button>Оформить заказ</button>
        </div>
    </div>

</div>