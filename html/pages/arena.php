<div class="Arena">
    <div class="Arena-Description">

        <p>
            Автоателье «Арена-Тюнинг» с 2004 года занимает одну из ведущих позиций на российском рынке в области
            производства, продажи, установки и ремонта аэродинамических обвесов и композитных деталей кузова для
            автомобилей
            премиум-класса. Автодизайн и тюнинг направлены на улучшение заводских характеристик и создание
            индивидуального
            неповторимого стиля. Специалисты нашей компании также окажут Вам профессиональные услуги по установке
            различных
            аксессуаров для Вашего автомобиля.
        </p>
        <img src="/img/pic/arena-car.jpg" alt=""/>
    </div>
    <?php
    require './modules/module-preview.php';
    ?>
    <section class="Arena-Hits">
        <h2>Хиты продаж</h2>
<!--    Module-Hits    -->
        <?php
        require './modules/module-hits.php';
        ?>
    </section>
    <section class="Arena-Hits">
        <h2>Хиты продаж</h2>
        <!--    Module-Hits    -->
        <?php
        require './modules/module-hits.php';
        ?>
        <?php
        require './modules/modal-form.php';
        ?>
    </section>
    <ul class="Arena-Menu">
        <li>

                <svg><use xlink:href="#package"></use></svg>
                Склад

        </li>
        <li>

                <svg><use xlink:href="#call-center"></use></svg>
                Сервис

        </li>
        <li>

                <svg><use xlink:href="#delivery"></use></svg>
                Доставка

        </li>
        <li>

                <svg><use xlink:href="#wallet"></use></svg>
                Цена

        </li>
        <li>

                <svg><use xlink:href="#24-hours"></use></svg>
                Гарантия

        </li>
    </ul>
</div>
