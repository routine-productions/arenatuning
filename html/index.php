<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="./css/index.min.css" type="text/css"/>
    <link rel="apple-touch-icon" sizes="57x57" href="/img/fav/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/fav/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/fav/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/fav/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/fav/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/fav/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/fav/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/fav/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/fav/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/img/fav/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/img/fav/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/img/fav/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/img/fav/manifest.json">
    <link rel="mask-icon" href="/img/fav/safari-pinned-tab.svg" color="#696969">
    <link rel="shortcut icon" href="/img/fav/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/fav/mstile-144x144.png">
    <meta name="msapplication-config" content="/img/fav/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

</head>
<body>
    <?php
    require_once './modules/header.php';
    ?>
    <div class="Car-Tune"></div>
<main>
    <!--        Внутренняя страница-->
    <?php
    if ($_SERVER['REQUEST_URI'] == '/') {
        require_once './pages/arena.php';
    } else {
        require_once './pages/' . $_SERVER['REQUEST_URI'] . '.php';
    }
    ?>
    <!--        Сайдбар -->
    <?php
    require_once './modules/sidebar.php';
    ?>
</main>
    <?php
    require_once './modules/footer.php';
    ?>
<!--<script src="index.min.js"></script>-->
<!--<script src="jquery.actual.min.js"></script>-->


</body>
</html>

