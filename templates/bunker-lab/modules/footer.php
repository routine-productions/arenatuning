<footer>
    <div class="Footer-Top">
        <div class="Footer-Left">
            <ul>
                <li><a href="/">Главная</a></li>
                <li><a href="/company/">О компании</a></li>
                <li><a href="/contacts/">Контакты</a></li>
            </ul>
            <ul>
                <li><a href="/payment/">Оплата и доставка</a></li>
                <li><a href="/montage/">Установка и покраска</a></li>
                <li><a href="/portfolio/">Портфолио</a></li>
            </ul>
            <a href="/" class="Footer-Logo">
                <svg>
                    <use xlink:href="#Logo"></use>
                </svg>
            </a>
        </div>
        <div class="Footer-Right">
            <div>
                <a>
                    <svg>
                        <use xlink:href="#room"></use>
                    </svg>
                    <span>Московская обл., Мытищи, ул. Колпакова д. 2.</span>
                </a>
            </div>
            <div>
                <a>
                    <svg>
                        <use xlink:href="#phone"></use>
                    </svg>
                    <span><color>+7 (495) 902-53-83</color> (9:00 - 19:00)</span>
                </a>
            </div>
            <div>
                <a href="/contacts">
                    <svg>
                        <use xlink:href="#near"></use>
                    </svg>
                    <span>Схема проезда</span>
                </a>
            </div>
            <div>
                <a href="mailto:zakaz@arenatuning.ru">
                    <svg>
                        <use xlink:href="#email"></use>
                    </svg>
                    <span>zakaz@arenatuning.ru</span>
                </a>
            </div>
        </div>
    </div>
    <div class="Copyright-Wrap">
        <div class="Copyright">
            <span>&copy; 2016 Арена&ndash;Тюнинг</span>
            <span class="Right">Все права защищены</span>
        </div>
    </div>
</footer>