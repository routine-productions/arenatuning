<aside class="Sidebar">
    <nav>
        <div class="Sidebar-Cell">
            <h3>Каталог товаров</h3>
            <ul class="Sidebar-Catalog">
                <jdoc:include type="modules" name="catalog"/>
            </ul>
        </div>

        <jdoc:include type="modules" name="sidebar"/>

        <div class="Sidebar-Info">
            <h3>Информация</h3>
            <ul>
                <!--                <li>-->
                <!--                    <a href="/stock/">-->
                <!--                        <svg>-->
                <!--                            <use xlink:href="#stock"></use>-->
                <!--                        </svg>-->
                <!--                        Акции</a>-->
                <!--                </li>-->
                <li>

                    <a href="/feedback/">
                        <svg>
                            <use xlink:href="#comment"></use>
                        </svg>
                        Отзывы клиентов</a>
                </li>
                <li>

                    <a href="/news/">
                        <svg>
                            <use xlink:href="#chat-bubble"></use>
                        </svg>
                        Новости</a>
                </li>
                <li>
                    <a href="support">
                        <svg>
                            <use xlink:href="#forum"></use>
                        </svg>
                        F.A.Q.</a>
                </li>
            </ul>
        </div>
    </nav>
</aside>