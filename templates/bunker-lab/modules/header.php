<header class="Site-Header JS-Mobile-Menu">
    <div class="Header-Top">
        <div class="Header-Logo">
            <a href="/">
                <svg>
                    <use xlink:href="#Logo"></use>
                </svg>
            </a>
        </div>
        <a href="#" class="JS-Mobile-Menu-Toggle">
            <svg>
                <use xlink:href="#Hamburger"></use>
            </svg>
        </a>
        <div class="Header-Info">
            <div class="Header-Call">
                <svg>
                    <use xlink:href="#phone"></use>
                    <dl>
                        <dt>Позвоните нам</dt>
                        <dd>8 (495) 902-53-83</dd>
                    </dl>
                </svg>
            </div>
            <div class="Header-Work">
                <svg>
                    <use xlink:href="#clock"></use>
                </svg>
                <dl>
                    <dt>Время работы</dt>
                    <dd>Пн-Вс 9:00 - 19:00</dd>
                </dl>
            </div>

            <?php if (!empty($City)) { ?>
                <div class="Header-City">
                    <svg>
                        <use xlink:href="#room"></use>
                    </svg>
                    <dl>
                        <dt>Ваш город</dt>
                        <dd><?= $City['city']['name_ru'] ?></dd>
                    </dl>
                </div>
            <?php } ?>

        </div>
    </div>
    <div class="Header-Bottom">
        <nav class="JS-Mobile-Menu-List">
            <ul>
                <li><a href="/">Главная</a></li>
                <li><a href="/company/">О компании</a></li>
                <li><a href="/payment/">Оплата и доставка</a></li>
                <li><a href="/montage/">Установка и покраска</a></li>
                <li><a href="/portfolio/">Портфолио</a></li>
                <li><a href="/contacts/">Контакты</a></li>
                <li><a class="Basket" href="/basket/">Корзина</a></li>
            </ul>
            <div class="Header-Right">
                <div class="Header-Search">
                    <jdoc:include type="modules" name="search"/>
                </div>
                <a href="/basket/">
                    <svg>
                        <use xlink:href="#basket"></use>
                    </svg>
                </a>
            </div>
        </nav>
    </div>

</header>