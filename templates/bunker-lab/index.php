﻿<?php
require_once(__DIR__ . "/../../includes/sxgeo.php");
$SxGeo = new SxGeo('SxGeoCity.dat');
$City = $SxGeo->getCity($_SERVER['REMOTE_ADDR']);

$Doc = JFactory::getDocument();
$Title = $Doc->getTitle();
if (!empty($City)) {
    $Title .= ' в городе ' . $City['city']['name_ru'];
}

$Doc->setTitle($Title);
?>


<!DOCTYPE html>
<html>
<head lang="ru">
    <jdoc:include type="head"/>
    <meta charset="UTF-8">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,300italic,500,700&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>

    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <link rel="stylesheet" href="./css/index.min.css" type="text/css"/>
    <link rel="apple-touch-icon" sizes="57x57" href="/img/fav/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/fav/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/fav/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/fav/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/fav/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/fav/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/fav/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/fav/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/fav/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/img/fav/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/img/fav/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/img/fav/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/img/fav/manifest.json">
    <link rel="mask-icon" href="/img/fav/safari-pinned-tab.svg" color="#696969">
    <link rel="shortcut icon" href="/img/fav/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/fav/mstile-144x144.png">
    <meta name="msapplication-config" content="/img/fav/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <?php JHtml::_('bootstrap.tooltip'); ?>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter31706821 = new Ya.Metrika({
                        id: 31706821,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true
                    });
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/31706821" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
</head>

<body>

<?php
require_once __DIR__ . '/../../img/sprite.svg';
?>

<?php
require_once __DIR__ . '/modules/header.php';
?>


<div class="JS-Carousel Car-Tune">
    <div class="JS-Carousel-List">
        <div class="Car-Tune JS-Carousel-Item" style="background-image: url(/img/pic/car.jpg);">
            <a href="http://arena/component/jshopping/audi?Itemid=0">У нас есть обвесы для AUDI</a>
        </div>
        <div class="Car-Tune JS-Carousel-Item" style="background-image: url(/img/pic/bmw-1.jpg);">
            <a href="http://arena/component/jshopping/bmw?Itemid=0">У нас есть обвесы для BMW</a>
        </div>
    </div>
</div>

<main>


    <div class="Right-Block">
        <jdoc:include type="modules" name="bread-crumbs"/>

        <?php if ($_SERVER["REQUEST_URI"] != '/') { ?>
            <!--            <jdoc:include type="message"/>-->
            <jdoc:include type="component"/>
        <?php } ?>

        <jdoc:include type="modules" name="after-component"/>

        <?php if ($_SERVER["REQUEST_URI"] == '/') { ?>
            <div class="Home-Payment">
                <h2 class="Hits-Header">Оплата и доставка</h2>

                <p>Мы принимаем оплату любыми удобными для Вас способами.</p>
                <div class="Payments">
                    <img src="/img/payments/alfabank-logo.png" alt="">
                    <img src="/img/payments/sberbank.png" alt="">
                    <img src="/img/payments/visa.png" alt="">
                    <img src="/img/payments/mastercard.png" alt="">
                    <img src="/img/payments/qiwi_wallet.png" alt="">
                    <img src="/img/payments/yandex_money.png" alt="">
                </div>

                <p>В город <?= $City['city']['name_ru'] ? $City['city']['name_ru'] : 'Москва' ?> и по области, а также
                    другие регионы РФ,
                    товар доставляется курьером или
                    самовывозом. В случае Вашего нахождения в другом регионе России мы отправляем груз транспортными
                    компаниями или по почте. Расширяя дилерскую сеть мы стремимся к повышению эффективности работы с
                    магазинами тюнинга и оптовыми продавцами автомобильных аксессуаров. Нашим представителям и
                    региональным дилерам для удобства пользования сайтом рекомендуем зарегистрироваться. Процедура
                    регистрации занимает не более 2 - 3 минут, но это дает возможность на стадии формирования
                    заказа, не открывая прайс, видеть дилерские цены, а также получать сообщения о новинках и
                    спецпредложениях . Условия нашего сотрудничества не обязывают приобретать обвесы оптом. Для
                    предоставления льготных условий закупок достаточно быть нашим постоянным и надежным партнером.
                    Мы заинтересованы в обоюдно выгодных условиях сотрудничества!</p>
                <p>Определяйтесь с составом заказа. Оформляйте заказ на сайте. При заказе обязательно укажите точный
                    адрес доставки, свою фамилию имя отчество, номер телефона, модель вашего автомобиля . Получаете
                    счет на ваш почтовый ящик. После подтверждения заказа менеджером,распечатывайте его(или
                    перепишите реквизиты) и оплачивайте в любом банке. После чего ожидайте получения долгожданного
                    комплекта от 5 до 15 дней(в среднем).</p>
                <p>Наша служба доставки обязуется сделать все возможное для того, чтобы адресат, находящийся в любом
                    регионе
                    России или в странах Таможенного союза (Беларусь, Казахстан) получил свой заказ в кратчайшие сроки.
                    Отгрузка товаров с нашего склада производится ежедневно.</p>
                <p>Мы предоставляем бесплатную доставку заказов до транспортных компаний, независимо от их объемов.
                    Данные
                    по весу и габаритам груза Вы можете уточнить у менеджеров компании. Сроки и стоимость доставки до
                    пункта
                    назначения рассчитываются согласно тарифам, установленным транспортными компаниями. Мы работаем
                    только с
                    надежными перевозчиками:</p>
                <ul>
                    <li>Желдорэкспедиция</li>
                    <li>Энергия</li>
                    <li>Деловые линии</li>
                    <li>Автотрейдинг</li>
                    <li>ПЭК</li>
                </ul>
                <p>Оплата услуг транспортных компаний производится заказчиком после того, как товар будет получен им в
                    пункте назначения.</p>
                <img src="/img/pic/map.jpg" alt="">
            </div>
        <?php } ?>
    </div>

    <?php require_once __DIR__ . '/modules/sidebar.php'; ?>
</main>
<?php
require_once __DIR__ . '/modules/footer.php';
?>


<script src="index.min.js"></script>
</body>
</html>
