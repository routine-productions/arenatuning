<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_breadcrumbs
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
?>
<nav class="Breadcrumbs">
    <ul>
        <?php
        $show_last = $params->get('showLast', 1);

        foreach ($list as $key => $item) :
            if ($key != count($list) - 1) :?>
                <li>
                    <?php if (!empty($item->link)) : ?>
                        <a href="<?php echo $item->link; ?>"><?php echo $item->name; ?></a>
                    <?php else : ?>
                        <a>
                            <?php echo $item->name; ?>
                        </a>
                    <?php endif; ?>
                </li>
            <?php elseif ($show_last) : ?>
                <li>
                    <a class="Active">
                        <?php echo $item->name; ?>
                    </a>
                </li>
            <?php endif;
        endforeach; ?>
    </ul>
</nav>