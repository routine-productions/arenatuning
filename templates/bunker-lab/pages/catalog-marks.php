<div class="Catalog">
    <nav>
        <div class="Catalog-Logo"><div></div></div>
        <ul>
            <li><a href="/">Главная</a></li>
            <li><a class="Active" href="catalog-marks">Range Rover</a></li>
        </ul>
    </nav>
    <div class="Catalog-Mark">
        <!--    MODULE-FEATURE    -->
        <?php
        for ($Index = 0; $Index < 3; $Index++) {
            require './modules/module-feature.php';
        }
        ?>
    </div>
    <div class="Arena-Hits">
        <h2>Хиты продаж</h2>
        <!--    Module-Hits    -->
        <?php
        require './modules/module-hits.php';
        ?>
    </div>
    </div>