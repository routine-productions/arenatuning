<div class="Contacts">
    <div class="Contact-Info">
        <div class="Info-Row">
            <div>
                <svg>
                    <use xlink:href="#room"></use>
                </svg>
            </div>

            <p>Московская обл., Мытищи, ул. Колпакова, дом 2</p>
        </div>
        <div class="Info-Row">
            <div>
                <svg>
                    <use xlink:href="#phone"></use>
                </svg>
            </div>
            <p>8 (495) 902-53-83</p>
        </div>
        <div class="Info-Row">
            <div>
                <svg>
                    <use xlink:href="#email"></use>
                </svg>
            </div>
            <p><a href="mail-to:">zakaz@arenatuning.ru</a>
            </p>
        </div>
        <div class="Info-Row">
            <div>
                <svg>
                    <use xlink:href="#clock"></use>
                </svg>
            </div>
            <p>Пн-Вс. 9:00 - 19:00</p>
        </div>
        <div class="Info-Row">
            <div>
                <svg>
                    <use xlink:href="#near"></use>
                </svg>
            </div>

            <p>От М Медведково, маршрутка №177 до ост. «Рубин», далее пешком 300 метров по ул. Колпакова до технопарка
                «Новое Время».
                <span>От платформы Мытищи, маршрутки № 4, 13, 11, 20 до ост. «Магазин Рубин», далее пешком 300 метров по ул. Колпакова до технопарка «Новое Время».</span>
            </p>

        </div>
    </div>
    <div class="Contact-Form">
        <input placeholder="Имя" type="text"/>
        <input placeholder="E-mail" type="text"/>
        <textarea placeholder="Сообщение"></textarea>
        <button>Отправить</button>
    </div>
    <div class="Contact-Map">
        <script type="text/javascript" charset="utf-8"
                src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=CDzrFEB0l8fgVJJeZgGFOH4Pz-XpSu6H&width=100%&height=400&lang=ru_UA&sourceType=constructor"></script>
    </div>
    <p>Мы осуществляем отправку товара во все регионы России, а также в страны СНГ. Срок доставки от 2 до 10 дней. Мы
        заинтересованы в том, чтобы наш товар как можно быстрее оказался в Вашем городе. Хорошая репутация для нас - это
        главное! Все товары перед отправкой подлежат тщательной проверке, таким образом мы снижаем риск брака до нуля!
        Так же на каждый товар мы прилагаем гарантию от 1 до 3 лет.</p>

    <p>Если Вы не нашли нужный обвес, возможно мы сможем его доставить под заказ. Задайте вопрос консультанту о
        приобретении. Вам не понравилась ни одна модель обвеса для вашей машины, представленного в каталоге? Закажите
        эксклюзивный обвес по индивидуальному проекту!</p>

    <p>Отправка товара осуществляется после 100% предоплаты!</p>

    <p>Если Вы не нашли ответа на свой вопрос, свяжитесь с нашим on-line консультантом или позвоните нам.</p>

</div>