<div class="Catalog">
    <nav>
        <div class="Catalog-Logo">
            <div></div>
        </div>
        <ul>
            <li><a href="/">Главная</a></li>
            <li><a href="catalog-marks">Range Rover</a></li>
            <li><a href="catalog-mark">Range Rover Sport</a></li>
            <li><a href="catalog-items">Аэродинамические обвесы</a></li>
            <li><a class="Active" href="catalog-item">Комплект HAMANN Counqueror для Range Rover Sport</a></li>
        </ul>
    </nav>
    <section class="Catalog-Item">
        <h2>Комплект HAMANN Counqueror для Range Rover Sport</h2>

        <div class="Catalog-View">
            <?php require_once './modules/module-view.php'; ?>
            <dt>Реснички</dt>
            <dd>
                <select name="" id=""></select>
                <svg><use xlink:href="#expand-more"></use></svg>
                нет (-3 000,00 руб)
            </dd>

            <dt>Реснички</dt>
            <dd>
                <select name="" id=""></select>
                <svg><use xlink:href="#expand-more"></use></svg>
                нет (-3 000,00 руб)
            </dd>
        </div>
        <?php require_once './modules/catalog-info.php'; ?>
    </section>
    <section class="Catalog-Delivery">
        <h2>
            <button>Оплата</button>
            <button class="Active">Доставка</button>
        </h2>
        <div>
            <p> По Москве и области товар доставляется курьером (оплата при по-лучении) или самовывозом. В случае Вашего
                нахождения в другом регионе России мы отправляем груз транспортными компаниями или по почте. Расширяя
                дилерскую сеть мы стремимся к повышению эффективности работы с магазинами тюнинга и оптовыми продавцами
                автомобильных аксессуаров. Нашим представителям и региональным дилерам для удобства пользования сайтом
                рекомендуем зарегистрироваться. Процедура регистрации занимает не более 2-3 минут, но это дает
                возможность
                на стадии формирования заказа, не открывая прайс, видеть дилерские цены, а также получать сообщения о
                новинках и спецпредложениях. Условия нашего сотрудничества не обязывают приобретать обвесы оптом. Для
                предоставления льготных условий закупок достаточно быть нашим постоянным и надежным партнером. Мы
                заинтересованы в обоюдно выгодных условиях сотрудничества!</p>

        </div>
        <div class="Active">
            <p> По Москве и области товар доставляется курьером (оплата при по-лучении) или самовывозом. В случае Вашего
                нахождения в другом регионе России мы отправляем груз транспортными компаниями или по почте. Расширяя
                дилерскую сеть мы стремимся к повышению эффективности работы с магазинами тюнинга и оптовыми продавцами
                автомобильных аксессуаров. Нашим представителям и региональным дилерам для удобства пользования сайтом
                рекомендуем зарегистрироваться. Процедура регистрации занимает не более 2-3 минут, но это дает
                возможность
                на стадии формирования заказа, не открывая прайс, видеть дилерские цены, а также получать сообщения о
                новинках и спецпредложениях. Условия нашего сотрудничества не обязывают приобретать обвесы оптом. Для
                предоставления льготных условий закупок достаточно быть нашим постоянным и надежным партнером. Мы
                заинтересованы в обоюдно выгодных условиях сотрудничества!</p>

            <p>Определяйтесь с составом заказа. Оформляйте заказ на сайте. При заказе обязательно укажите точный адрес
                доставки, свою фамилию имя отчество, номер телефона, модель вашего автомобиля. Получаете счет на ваш
                почтовый ящик. После подтверждения заказа менеджером,распечатывайте его (или перепишите реквизиты) и
                оплачивайте в любом банке. После чего ожидайте получения долгожданного комплекта от 5 до 15 дней (в
                среднем).</p>

            <p>Юридические и физические лица получают все необходимые финансовые документы.
            </p>

            <p> Цены на товары уже включают все налоги, но не включают стоимость доставки. При отправке по жд груз
                страхуется. При отправке компаниями грузоперевозок - груз страхуется по желанию клиента. Стоимость
                доставки
                вы можете рассчитать самостоятельно на сайтах компаний грузоперевозок, указанных ниже.
            </p>
        </div>
    </section>
    <section class="Catalog-Recommend">
        <h2>Рекомендуем посмотреть</h2>
        <!--    Module-Hits    -->
        <?php
        require './modules/module-hits.php';
        ?>
    </section>
</div>