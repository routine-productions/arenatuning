<?php
// Connect
function Connection($DB)
{
// Config
    $User = 'root';
    $Pass = '987975';
    try {
        $Connection = new PDO('mysql:host=localhost;dbname=' . $DB . ';charset=utf8', $User, $Pass);
        $Connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $Connection;
    } catch (PDOException  $Error) {
        echo "Error: " . $Error;
    }
}

function Set_Data($Arena_DB, $Data, $Table)
{
    $Values = "'" . implode("', '", $Data) . "'";
    $Keys = "`" . implode('`, `', array_keys($Data)) . "`";

    $Query = 'INSERT INTO `' . $Table . '` (' . $Keys . ') VALUES (' . $Values . ')';

    $Arena_DB->query($Query);
}

$Old_Db = Connection('arena-old');
$Arena_DB = Connection('arena');


// Get categories
$Products = $Old_Db->query("SELECT * FROM `product`")->fetchAll(PDO::FETCH_ASSOC);

//foreach ($Products as $Product) {
////    print_r($Product);
//
//    $Product['text'] = str_replace("'", '"', $Product['text']);
//    $Product['text'] = str_replace('uploads/', '/uploads/', $Product['text']);
//
//    if (!empty($Product['image'])) {
//        $Url = 'http://arenatuning.ru/uploads/product/' . $Product['image'];
//        $Img = __DIR__ . '/../components/com_jshopping/files/img_products/' . $Product['image'];
//        if ($File = file_get_contents($Url)) {
//            file_put_contents($Img, $File);
//        }
//    }

// Set Product table
//    $Data = [
//        'product_id' => $Product['id'],
//        'product_quantity' => $Product['inStock'],
//        'product_ean' => $Product['productCode'],
//        'average_rating' => $Product['rating'],
//        'product_date_added' => $Product['date'],
//        'product_publish' => $Product['hide'] ? 0 : 1,
//        'product_price' => $Product['price'],
//        'min_price' => $Product['price'],
//        'access' => 1,
//        'name_ru-RU' => $Product['name'],
//        'alias_ru-RU' => $Product['routeName'],
//        'description_ru-RU' => $Product['text'],
//        'meta_title_ru-RU' => $Product['title'],
//        'meta_description_ru-RU' => $Product['description'],
//        'meta_keyword_ru-RU' => $Product['keywords'],
//        'image' => $Product['image']
//    ];
//
//    Set_Data($Arena_DB, $Data, 'q8m4v_jshopping_products');
//    Set_Data($Arena_DB, $Data, 'q8m4v_jshopping_products_to_categories');


// Category to products
//    $Data = [
//        'product_id' => $Product['id'],
//        'category_id' => $Product['categoryId'],
//    ];
//    Set_Data($Arena_DB, $Data, 'q8m4v_jshopping_products_to_categories');

// Get main images
//    $Data = [
//        'product_id' => $Product['id'],
//        'image_name' => $Product['image'],
//        'name' => $Product['image']
//    ];
//    Set_Data($Arena_DB, $Data, 'q8m4v_jshopping_products_images');
//}


// Get images
$Images = $Old_Db->query("SELECT * FROM `product_photo`")->fetchAll(PDO::FETCH_ASSOC);

foreach ($Images as $Image) {
//    $Data = [
//        'product_id' => $Image['productId'],
//        'image_name' => $Image['image'],
//        'name' => $Image['image']
//    ];
//    Set_Data($Arena_DB, $Data, 'q8m4v_jshopping_products_images');
//
//    $Product['text'] = str_replace("'", '"', $Product['text']);
//    $Product['text'] = str_replace('uploads/', '/uploads/', $Product['text']);

//    if (!empty($Image['image'])) {
//        $Url = 'http://arenatuning.ru/uploads/photos/' . $Image['image'];
//
////        $Image['thumb'] = str_replace('thumb','thumb_',$Image['thumb']);
//        $Img = __DIR__ . '/../components/com_jshopping/files/temp/' . $Image['image'];
//        if ($File = file_get_contents($Url)) {
//            file_put_contents($Img, $File);
//        }
//    }
}


function transliterate($input)
{
    $gost = array(
        "Є" => "YE", "І" => "I", "Ѓ" => "G", "і" => "i", "№" => "-", "є" => "ye", "ѓ" => "g",
        "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D",
        "Е" => "E", "Ё" => "YO", "Ж" => "ZH",
        "З" => "Z", "И" => "I", "Й" => "J", "К" => "K", "Л" => "L",
        "М" => "M", "Н" => "N", "О" => "O", "П" => "P", "Р" => "R",
        "С" => "S", "Т" => "T", "У" => "U", "Ф" => "F", "Х" => "X",
        "Ц" => "C", "Ч" => "CH", "Ш" => "SH", "Щ" => "SHH", "Ъ" => "'",
        "Ы" => "Y", "Ь" => "", "Э" => "E", "Ю" => "YU", "Я" => "YA",
        "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d",
        "е" => "e", "ё" => "yo", "ж" => "zh",
        "з" => "z", "и" => "i", "й" => "j", "к" => "k", "л" => "l",
        "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
        "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "x",
        "ц" => "c", "ч" => "ch", "ш" => "sh", "щ" => "shh", "ъ" => "",
        "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
        " " => "_", "—" => "_", "," => "_", "!" => "_", "@" => "_",
        "#" => "-", "$" => "", "%" => "", "^" => "", "&" => "", "*" => "",
        "(" => "", ")" => "", "+" => "", "=" => "", ";" => "", ":" => "",
        "'" => "", "~" => "", "`" => "", "?" => "", "/" => "",
        "\"" => "", "[" => "", "]" => "", "{" => "", "}" => "", "|" => "", " " => ""
    );

    return strtr($input, $gost);
}


// Get Attributes Type
//$Attribute_Types = $Old_Db->query("SELECT * FROM `property_type`")->fetchAll(PDO::FETCH_ASSOC);
//
//foreach ($Attribute_Types as $Attribute_Type) {
//    $Data = [
//        'attr_id' => $Attribute_Type['id'],
//        'attr_type' => 1,
//        'independent' => 1,
//        'allcats' => 1,
//        'cats' => 'a:0:{}',
//        'name_ru-RU' => $Attribute_Type['name'],
//        'name_en-GB' => transliterate($Attribute_Type['name'])
//    ];
//
//    Set_Data($Arena_DB, $Data, 'q8m4v_jshopping_attr');
//}


// Get Attributes Values
//$Attributes = $Old_Db->query("SELECT * FROM `property`")->fetchAll(PDO::FETCH_ASSOC);
//
//foreach ($Attributes as $Attribute) {
//    $Data = [
//        'value_id' => $Attribute['id'],
//        'attr_id' => $Attribute['propertyTypeId'],
//        'name_ru-RU' => $Attribute['value'],
//        'name_en-GB' => transliterate($Attribute['value']),
//        'value_ordering' => transliterate($Attribute['pos']),
//    ];
//    Set_Data($Arena_DB, $Data, 'q8m4v_jshopping_attr_values');
//}
//

// Get Products Attributes
//$Products_Attributes = $Old_Db->query("SELECT * FROM `product_property`")->fetchAll(PDO::FETCH_ASSOC);
//
//foreach ($Products_Attributes as $Product_Attribute) {
//
//    $Attrbute = $Old_Db->query("SELECT * FROM `property` WHERE `id`='" . $Product_Attribute['propertyItemId'] . "'")
//        ->fetchAll(PDO::FETCH_ASSOC);
////print_r($Product_Attribute);
//
//    $Data = [
//        'id' => $Product_Attribute['id'],
//        'product_id' => $Product_Attribute['productId'],
//        'attr_id' => $Attrbute[0]['propertyTypeId'],
//        'attr_value_id' => $Product_Attribute['propertyItemId'],
//        'addprice' => $Product_Attribute['price'],
//        'price_mod' => '-'
//    ];
//    Set_Data($Arena_DB, $Data, 'q8m4v_jshopping_products_attr2');
//}

$Dir = __DIR__ . '/../components/com_jshopping/files/img_products/';
$Files = scandir($Dir);
print_r($Files);

foreach ($Files as $file) {
    copy($Dir . $file, $Dir . 'thumb_' . $file);
}